Algorithm

1. Landmark points -- src, dest

2. Interpolate src and dest points according to ratio t: in_between. Transform source image by doing a transformation from src to in_between. Transform source image by doing a transformation from in_between to dest.
a. Interpolation function is linear here.

3. Do color interpolation to blend the two intermediate images according to ratio t.
a. Interpolation function is quadratic... so that it will stay at the source face for a longer period of time.

4. Remapping using linear interpolation.

5. Local transformation. Calculate mesh using Delaunay triangulation, then do piecewise transformation on each triangle.

---

For global transform: poly is reasonable as it fits very well allowing for a seamless transition. but it may leading to extreme warping. The landmark points need to be designed very well! Thin-plate spline is good as it has least bending when transforming so less distortion. So we don't need to be so rigorous with the landmark points. Because it models the bending of a thin-metal plate, which is quite a similar model to our faces.

For local transform: thin-plate spline is good as deformation field is smooth.