#Face Morpher
#Mark two images. They will be warped and blended according to some geometric transform.

import cv
import numpy
import numpy.linalg as la
import tps #thin-plate spline
import math
from skimage import transform as tf #extra geometric transforms
from scipy import spatial
import pickle #for serialization

WIDTH = 600

def main():
    #sauce: http://pics.psych.stir.ac.uk/2D_face_sets.htm
    #faces = ["f4006s.jpg", "f4026s.jpg", "m4042s.jpg", "m4063s.jpg", "m4066s.jpg", "f4006s.jpg"] #loopyloop
    #faces = ["f4006s.jpg", "f4026s.jpg"]
    faces = ["if3008.jpg", "if902.jpg", "if3108.jpg", "if3110.jpg"]

    #imgfile1, imgfile2 = "angelababy.jpg", "karengillian.jpg"

    filename = "side_pw_tps_loop.avi"
    fps = 30
    width, height = WIDTH, 450
    outvid = cv.CreateVideoWriter(filename, 0, fps, (width, height), 1)
    #outvid = None

    for i in xrange(0, len(faces)-1):
        start_gui(faces[i], faces[i+1], outvid)

    print "{}".format(filename)
    del outvid


def start_gui(imgfile1, imgfile2, outvid):
    show_triangles = False

    gui = GUI(imgfile1, imgfile2)
    gui.show_first_time()
    if show_triangles:
        gui.show_triangles()

    if outvid == None:
        while True:
        	if cv.WaitKey(0) == 27:   # Wait for 0 ms and check key press. ASCII 27 is the ESC key.
        		break

    morphgui = gui.morphing_time(Transform.PW_TPS) #You can change the transform type
    morphgui.show_first_time()
    gui.destroy()

    if outvid != None:
        #if cv.WaitKey(0) == 27:
        morphgui.write_video(outvid, 0.1) #TODO support multiple files??!

    if outvid == None:
        cv.WaitKey(0)
    morphgui.close()
    del morphgui, gui

def search_point(ptlist, x, y, radius):
    """Search for a point in a list and return the index"""
    minx = x - radius
    maxx = x + radius
    miny = y - radius
    maxy = y + radius

    for i in range(len(ptlist)):
        px = ptlist[i][0]
        py = ptlist[i][1]
        if px >= minx and px <= maxx and py >= miny and py <= maxy:
		   return i, px, py

    return -1, -1, -1

def on_mouse_click(event, x, y, flags, param):
    """Mark image on mouse click"""
    window = param
    gui = window.gui

    if event == cv.CV_EVENT_LBUTTONUP: #and gui.focus == window:
        print x,y
        window.mark((x, y))
        #gui.switch_focus()

    if event == cv.CV_EVENT_RBUTTONDOWN: #and gui.focus == gui.win[0]:
        gui.remove_points(window, x, y, 6)

    return None

def cv2array(im):
    depth2dtype = {
        cv.IPL_DEPTH_8U: 'uint8',
        cv.IPL_DEPTH_8S: 'int8',
        cv.IPL_DEPTH_16U: 'uint16',
        cv.IPL_DEPTH_16S: 'int16',
        cv.IPL_DEPTH_32S: 'int32',
        cv.IPL_DEPTH_32F: 'float32',
        cv.IPL_DEPTH_64F: 'float64',
    }

    arrdtype=im.depth
    a = numpy.fromstring(
         im.tostring(),
         dtype=depth2dtype[im.depth],
         count=im.width*im.height*im.nChannels)
    a.shape = (im.height,im.width,im.nChannels)
    return a

def array2cv(a):
    dtype2depth = {
        'uint8':   cv.IPL_DEPTH_8U,
        'int8':    cv.IPL_DEPTH_8S,
        'uint16':  cv.IPL_DEPTH_16U,
        'int16':   cv.IPL_DEPTH_16S,
        'int32':   cv.IPL_DEPTH_32S,
        'float32': cv.IPL_DEPTH_32F,
        'float64': cv.IPL_DEPTH_64F,
    }
    try:
        nChannels = a.shape[2]
    except:
        nChannels = 1
    cv_im = cv.CreateImageHeader((a.shape[1],a.shape[0]),
          dtype2depth[str(a.dtype)],
          nChannels)
    cv.SetData(cv_im, a.tostring(),
             a.dtype.itemsize*nChannels*a.shape[1])
    return cv_im

### Helper object ###
class Transform:
    AFFINE = 1
    POLY = 2
    TPS = 3
    PW_AFFINE = 4
    PW_POLY = 5
    PW_TPS = 6

### GUI Objects ###

class Window:
    def __init__(self, gui, imagefname):
        self.gui = gui
        self.name = imagefname
        self.textfile = "{}.txt".format(imagefname)
        self.window = cv.NamedWindow(self.name, flags=0)
        self.ptlist = []
        image = cv.LoadImage(imagefname, cv.CV_LOAD_IMAGE_COLOR)

        cv.SetMouseCallback(self.name, on_mouse_click, self)

        #resize image (keep aspect ratio)
        width = WIDTH
        height = image.height * width/image.width
        resized_image = cv.CreateImage((width,height), 8, 3)
        cv.Resize(image, resized_image)

        cv.ResizeWindow(self.name, width, height)

        self.initial_image = resized_image
        self.image = cv.CloneImage(self.initial_image)

    def show_first_time(self):
        #check if there are any points to load
        if self.load_points():
            self.redraw()
        else:
            self.show()

    def draw_marker(self, point, no):
        color = (0,0,255,0)
        dist = 4 #distance of text from point
        x, y = point
        cv.Circle(self.image, point, 6, color, -1)
        cv.PutText(self.image, str(no), (x+dist,y+dist), self.gui.font, color)

    def mark(self, point):
        self.ptlist.append(point)
        self.draw_marker(point, len(self.ptlist))
        cv.ShowImage(self.name, self.image)

    def redraw(self):
        cv.Copy(self.initial_image, self.image)
        count = 1
        for point in self.ptlist:
            self.draw_marker(point, count)
            count += 1
        cv.ShowImage(self.name, self.image)

    def show(self):
        cv.ShowImage(self.name, self.image)

    def save_points(self):
        try:
            f = open(self.textfile, "w")
            pickle.dump(self.ptlist,f)
            f.close()
            print "Points saved to {}".format(self.textfile)
        except IOError:
            print "Cannot save points!"

    def load_points(self):
        try:
            f = open(self.textfile)
            self.ptlist = pickle.load(f)
            f.close()
            print "Loaded points: {}".format(self.ptlist)
            return True
        except IOError:
            print "No points to load!"
        return False

    def draw_triangles(self):
        color = (0,0,255,0)
        width = self.image.width
        height = self.image.height

        include_corners = False
        if include_corners:
            extras = [(0,0), (width, 0), (0,height), (width, height)] #corners of image -- maybe make it optional to use corners?
        else:
            extras = []

        ptlist = list(self.ptlist)
        ptlist += extras

        print "Tesselating"
        tesselation = spatial.Delaunay(ptlist)
        polygons = []
        for tri in tesselation.vertices:
            polygon = []
            for index in tri:
                polygon.append(self.ptlist[index])
            polygons.append(polygon)

        cv.PolyLine(self.image, polygons, 1, color)

        self.show()

class GUI:
    """The landmark points selection GUI"""
    def __init__(self, imgfname1, imgfname2):
        font_size = 0.8
        self.font = cv.InitFont(cv.CV_FONT_HERSHEY_SIMPLEX, font_size, font_size)
        self.win = [0, 0]
        self.win[0] = Window(self, imgfname1)
        self.win[1] = Window(self, imgfname2)
        self.focus = self.win[0]

    def show_first_time(self):
        for win in self.win:
            win.show_first_time()

    def show_triangles(self):
        for win in self.win:
            win.draw_triangles()

    def switch_focus(self):
        if self.focus == self.win[0]:
            self.focus = self.win[1]
        else:
            self.focus = self.win[0]

    def destroy(self):
        for win in self.win:
            win.save_points()
            cv.DestroyWindow(win.name)

    def remove_points(self, window, x, y, radius):
        index, x, y = search_point(window.ptlist, x, y, radius)
        print index
        for win in self.win:
            win.ptlist.pop(index)
            win.redraw()

    def separate_point_list(self, ptlist):
        x_list = []
        y_list = []
        for (x,y) in ptlist:
            x_list.append(x)
            y_list.append(y)
        return x_list, y_list

    def morphing_time(self, transform_type):
        morph = MorphGUI(self.win[0].initial_image, self.win[1].initial_image, self.win[0].ptlist, self.win[1].ptlist, transform_type)
        return morph

class MorphGUI:
    """GUI for morphing"""
    def __init__(self, src_image, dest_image, src_ptlist, dest_ptlist, transform_type):
        self.name = "Morphing Time"
        self.window = cv.NamedWindow(self.name, flags=0)
        self.trackbarName = "Percentage"

        self.src_image = src_image
        self.dest_image = dest_image
        self.src_ptlist = src_ptlist
        self.dest_ptlist = dest_ptlist

        self.height = max(src_image.height, dest_image.height)
        self.width = WIDTH

        self.transform_type = transform_type

        self.prepare_for_transform() #prepares anything that can be prepared beforehand to make transform faster

        cv.CreateTrackbar(self.trackbarName, self.name, 0, 100, self.on_slide) #slider to control time of warp

        #pad smaller image
        smaller_image = src_image
        if src_image.height == self.height:
            smaller_image = dest_image
        resized_image = cv.CreateImage((self.width,self.height), 8, 3)
        rect = (0,0, smaller_image.width, smaller_image.height)
        cv.SetImageROI(resized_image, rect)
        cv.Copy(smaller_image, resized_image)
        cv.ResetImageROI(resized_image)
        cv.ResizeWindow(self.name, self.width, self.height)

        if src_image.height == self.height:
            self.dest_image = resized_image
        else:
            self.src_image = resized_image

    def show_first_time(self):
        self.show(self.src_image)

    def close(self):
        cv.DestroyWindow(self.name)

    def on_slide(self, pos):
        x = pos/100.0
        print "Ratio: {}".format(x)
        result = self.morphing_time(x)

        self.show(result)
        print "Showed image."

    def show(self, image):
        cv.ShowImage(self.name, image)

    ### Helper functions ###

    def color_function(self, ratio):
        """Use some other function for interpolation"""
        return pow(ratio, 2)

    def color_interpolate(self, image1, image2, ratio):
		#sum = cv.CreateMat(self.height, self.width, cv.CV_8UC3)
        sum = cv.CreateImage((self.width,self.height), 8, 3)
        cv.AddWeighted(image1, 1-ratio, image2, ratio, 0.0, sum)
        return sum

    def interpolate_point(self, tuple1, tuple2, ratio):
        """Precond: tuple1 and tuple2 have same len"""
        final = []
        for i in xrange(len(tuple1)):
            final.append((1-ratio)*tuple1[i] + ratio*tuple2[i])
        return tuple(final)

    def interpolate_points(self, src, dest, ratio):
        """For each src point in src ptlist, calculate weighted combination of src and dest point in dest ptlist"""
        final = []
        for i in xrange(len(src)):
            final.append(self.interpolate_point(src[i], dest[i], ratio))
        return final

    def prepare_for_transform(self):
        transform = self.transform_type
        if transform == Transform.POLY or transform == Transform.PW_POLY:
            self.poly_matrix = self.poly_matrix()
        elif transform == Transform.AFFINE:
            self.t_map = self.affine_transform(self.src_ptlist, self.dest_ptlist)
            self.inv_t_map = self.affine_transform(self.dest_ptlist, self.src_ptlist)

    ### Transformation functions ###

    def pers_transform(self, src_ptlist, dest_ptlist):
        t_map = cv.CreateMat(3, 3, cv.CV_32F)
        cv.GetPerspectiveTransform(src_ptlist, dest_ptlist, t_map)
        return t_map

    def affine_transform(self, src_ptlist, dest_ptlist):
        t_map = cv.CreateMat(2, 3, cv.CV_32F)
        cv.GetAffineTransform(src_ptlist, dest_ptlist, t_map)
        return t_map

    def affine_time(self, ratio):
        """Warps an image by an affine transform (for now)
        More points, better fit?"""
        image_sum = cv.CreateMat(2, 3, cv.CV_32F)
        inv_image_sum = cv.CreateMat(2, 3, cv.CV_32F)

        identity = cv.CreateMat(2, 3, cv.CV_32F)
        cv.SetIdentity(identity)

        cv.AddWeighted(identity, 1-ratio, self.t_map, ratio, 0.0, image_sum)
        cv.AddWeighted(self.inv_t_map, 1-ratio, identity, ratio, 0.0, inv_image_sum)

        warp_image = cv.CreateImage((self.width,self.height), 8, 3)
        inv_warp_image = cv.CreateImage((self.width,self.height), 8, 3)
        final_image = cv.CreateImage((self.width,self.height), 8, 3)

        cv.WarpAffine(self.src_image, warp_image, image_sum)
        cv.WarpAffine(self.dest_image, inv_warp_image, inv_image_sum)

        cv.AddWeighted(warp_image, 1-ratio, inv_warp_image, ratio, 0.0, final_image)

        final_image = self.color_interpolate(final_image, inv_warp_image, ratio)

        return final_image

    def poly(self, x, y, degree=2):
        """Returns a polynomial list given x and y
        Degree is currently 2 only. Can be extended for any degree
        """
        poly = (x*x, y*y, x*y, x, y, 1)
        return poly

    def poly_transform(self, src, dest, degree=2):
        """Given two sets of landmark points, calculate transformation matrix"""
        #Create rows of polynomial function
        poly_rows = []
        for pt in src:
            poly_rows.append(self.poly(pt[0], pt[1]))
        poly_matrix = numpy.asmatrix(poly_rows)

        #Create u matrix and v matrix
        u_rows, v_rows = [], []
        for pt in dest:
            u_rows.append([pt[0]])
            v_rows.append([pt[1]])
        u_matrix, v_matrix = numpy.asmatrix(u_rows), numpy.asmatrix(v_rows)

        x1,e,r,s = la.lstsq(poly_matrix, u_matrix)
        x2,e,r,s = la.lstsq(poly_matrix, v_matrix)
        transform_matrix = numpy.concatenate((x1.T, x2.T), axis=0) #vertical stacking

        return transform_matrix

    def poly_matrix(self):
        """Create polynomial matrices -- need to optimize! Slow and might take up too much memory?"""
        print "Calculating polynomial matrices..."
        height, width = self.height, self.width
        poly_array = []
        #create poly array
        for x in xrange(width):
            poly_row = []
            for y in xrange(height):
                poly_row.append(self.poly(x,y))
            poly_array.append(poly_row)
        #set to float so that numpy.dot is optimized
        return numpy.asarray(poly_array, float, 'C') #this copying takes 1.9s here... quite long. How to optimize?

    def poly_warp(self, src_image, src_ptlist, dest_ptlist):
        """Warp the image, given some corresponding landmark points
        Do reverse mapping -- given a dest pt, we find the src mapping.
        """
        width = src_image.width
        height = src_image.height
        mapx = cv.CreateMat(height, width, cv.CV_32FC1)
        mapy = cv.CreateMat(height, width, cv.CV_32FC1)
        t_map = self.poly_transform(dest_ptlist, src_ptlist) #opposite direction. this is for reverse mapping.
        #print t_map

        #rows of tranformation matrix
        t_map_list = t_map.tolist()
        x_matrix = numpy.asarray(t_map_list[0], float, 'C')
        y_matrix = numpy.asarray(t_map_list[1], float, 'C')

        #Calculating transform...
        #making them C_CONTIGUOUS speeds up dot(), although making of numpy arrays take a long time!
        #defensive -- need to check for poly_matrix's existence?
        u_array = numpy.dot(self.poly_matrix, x_matrix)
        v_array = numpy.dot(self.poly_matrix, y_matrix)

        #Copying values...
        for x in xrange(width):
                    for y in xrange(height):
                        cv.Set2D(mapx, y, x, float(u_array[x][y]))
                        cv.Set2D(mapy, y, x, float(v_array[x][y]))

        #Do a remap
        final_image = cv.CreateImage((width,height), 8, 3)
        cv.Remap(src_image, final_image, mapx, mapy)

        return final_image

    def poly_time(self, ratio):
        """Returns a blend of images given a ratio, using polynomial transform"""
        between_ptlist = self.interpolate_points(self.src_ptlist, self.dest_ptlist, ratio)
        warp_image = self.poly_warp(self.src_image, self.src_ptlist, between_ptlist)
        inv_warp_image = self.poly_warp(self.dest_image, self.dest_ptlist, between_ptlist)

        final_image = self.color_interpolate(warp_image, inv_warp_image, self.color_function(ratio))

        if ratio == 1:
            return self.dest_image
        elif ratio == 0:
            return self.src_image

        return final_image

    def piecewise_poly_warp(self, src_image, src_ptlist, dest_ptlist):
        """From scikit-image transform _geometric.py
        We need to find the INVERSE map!
        """
        width = src_image.width
        height = src_image.height
        mapx = cv.CreateMat(height, width, cv.CV_32FC1)
        mapy = cv.CreateMat(height, width, cv.CV_32FC1)

        include_corners = True
        if include_corners:
            extras = [(0,0), (self.width, 0), (0,self.height), (self.width, self.height)] #corners of image -- maybe make it optional to use corners?
        else:
            extras = []

        #src = numpy.array(src_ptlist + extras)
        #dst = numpy.array(dest_ptlist + extras)
        src_ptlist += extras
        dest_ptlist += extras

        print "Tesselation and transformation matrices..."
        tesselation = spatial.Delaunay(src_ptlist)
        polys = []
        for tri in tesselation.vertices:
            #add the separate transforms for each triangle
            tri_src, tri_dest = [], []
            for index in tri:
                tri_src.append(src_ptlist[index])
                tri_dest.append(dest_ptlist[index])

            t_map = self.poly_transform(tri_dest, tri_src) #remember, we need to get the inverse for reverse mapping
            polys.append(t_map)

        #now, let's apply the transform to all triangles.
        #rows of tranformation matrix
        #assume order is guaranteed for now!*
        u_array_list, v_array_list = [], []
        for i in xrange(len(polys)):
            t_map = polys[i]
            t_map_list = t_map.tolist()
            x_matrix = numpy.asarray(t_map_list[0], float, 'C')
            y_matrix = numpy.asarray(t_map_list[1], float, 'C')
            u_array = numpy.dot(self.poly_matrix, x_matrix)
            v_array = numpy.dot(self.poly_matrix, y_matrix)
            u_array_list.append(u_array)
            v_array_list.append(v_array)

        print "Transform..."
        #Copying values...
        for x in xrange(width):
                    for y in xrange(height):
                        #find the corresponding triangle in dest image
                        simplex = tesselation.find_simplex((x,y))
                        if simplex == -1:
                            #coordinate out of mesh and not in any triangle
                            cv.Set2D(mapx, y, x, -1)
                            cv.Set2D(mapy, y, x, -1)
                            continue

                        u_array = u_array_list[simplex]
                        v_array = v_array_list[simplex]
                        cv.Set2D(mapx, y, x, float(u_array[x][y]))
                        cv.Set2D(mapy, y, x, float(v_array[x][y]))

        #Do a remap
        final_image = cv.CreateImage((width,height), 8, 3)
        cv.Remap(src_image, final_image, mapx, mapy)

        return final_image

    def piecewise_poly_time(self, ratio):
        between_ptlist = self.interpolate_points(self.src_ptlist, self.dest_ptlist, ratio)
        warp_image = self.piecewise_poly_warp(self.src_image, self.src_ptlist, between_ptlist)
        inv_warp_image = self.piecewise_poly_warp(self.dest_image, self.dest_ptlist, between_ptlist)

        final_image = self.color_interpolate(warp_image, inv_warp_image, self.color_function(ratio))

        return final_image

    def piecewise_affine_warp(self, src_image, src_ptlist, dest_ptlist):
        tform = tf.PiecewiseAffineTransform()
        extras = [(0,0), (self.width, 0), (0,self.height), (self.width, self.height)]
        src = numpy.array(src_ptlist + extras)
        dest = numpy.array(dest_ptlist + extras)
        tform.estimate(src, dest)

        image = cv2array(src_image)
        warp = tf.warp(image, tform)

        return cv.fromarray(warp)

    def piecewise_affine_time(self, ratio):
        between_ptlist = self.interpolate_points(self.src_ptlist, self.dest_ptlist, ratio)
        warp_image = self.piecewise_affine_warp(self.src_image, self.src_ptlist, between_ptlist)
        inv_warp_image = self.piecewise_affine_warp(self.dest_image, self.dest_ptlist, between_ptlist)

        final_image = self.color_interpolate(warp_image, inv_warp_image, self.color_function(ratio))

        return final_image


    def poly_scikit_warp(self, src_image, src_ptlist, dest_ptlist):
        tform = tf.PolynomialTransform()
        src = numpy.array(src_ptlist, dtype=object)
        dest = numpy.array(dest_ptlist, dtype=object) #dtype needed to prevent overflow during exponential ops

        tform.estimate(src, dest, 4)

        image = cv2array(src_image)
        coords = tf.warp_coords(tform, image.shape)

        warp = tf.warp(image, tform, {},(self.height, self.width))

        return cv.fromarray(warp) #array2cv(warp)

    def poly_scikit_time(self, ratio):
        """UNFINISHED - use scikit algo"""
        final_image = self.poly_scikit_warp(self.src_image, self.src_ptlist, self.dest_ptlist)

        return final_image

    def tps_warp(self, src_image, src_ptlist, dest_ptlist):
        """TPS warp src_image and returns warped image"""
        width = src_image.width
        height = src_image.height

        transform = tps._make_inverse_warp(src_ptlist, dest_ptlist, (0,0,width,height), 1) #remember, it's inverse warp...
        #we should invert height and width, then transpose transform
        #TODO: optimize by changing approximate grid
        #transform contains [mapx, mapy]

        #convert the mapping arrays
        mapx = cv.CreateMat(height, width, cv.CV_32FC1)
        mapy = cv.CreateMat(height, width, cv.CV_32FC1)
        #for some reason, works only if x and y are switched?!
        #tranpose fix https://code.ros.org/trac/opencv/ticket/357
        transform0 = transform[0].transpose().copy()
        transform1 = transform[1].transpose().copy()
        cv.Convert(cv.fromarray(transform0), mapx)
        cv.Convert(cv.fromarray(transform1), mapy)

        #remap!
        final_image = cv.CreateImage((width,height), 8, 3)
        cv.Remap(src_image, final_image, mapx, mapy)
        return final_image

    def tps_time(self, ratio):
        between_ptlist = self.interpolate_points(self.src_ptlist, self.dest_ptlist, ratio)
        warp_image = self.tps_warp(self.src_image, self.src_ptlist, between_ptlist)
        inv_warp_image = self.tps_warp(self.dest_image, self.dest_ptlist, between_ptlist)

        final_image = self.color_interpolate(warp_image, inv_warp_image, self.color_function(ratio))

        return final_image

    def piecewise_tps_warp(self, src_image, src_ptlist, dest_ptlist):
        """Calculates a mesh using feature points through Delaunay, then calculates separate transforms on each triangle

        From scikit-image transform _geometric.py
        We need to find the INVERSE map!
        """
        width = src_image.width
        height = src_image.height
        mapx = cv.CreateMat(height, width, cv.CV_32FC1)
        mapy = cv.CreateMat(height, width, cv.CV_32FC1)

        include_corners = True
        if include_corners:
            extras = [(0,0), (self.width, 0), (0,self.height), (self.width, self.height)] #corners of image -- maybe make it optional to use corners?
        else:
            extras = []

        #src = numpy.array(src_ptlist + extras)
        #dst = numpy.array(dest_ptlist + extras)
        src_ptlist += extras
        dest_ptlist += extras

        print "Tesselating and calculating inverse maps..."
        tesselation = spatial.Delaunay(src_ptlist)
        t_maps = []
        for tri in tesselation.vertices:
            #add the separate transforms for each triangle
            tri_src, tri_dest = [], []
            for index in tri:
                tri_src.append(src_ptlist[index])
                tri_dest.append(dest_ptlist[index])

            #maybe can improve speed by setting last argument to more than 1
            t_map = tps._make_inverse_warp(tri_src, tri_dest, (0,0,width,height), 1) #remember, we need to get the inverse for reverse mapping
            t_map[0] = t_map[0].transpose().copy()
            t_map[1] = t_map[1].transpose().copy()
            t_maps.append(t_map)

        print "Calculating remapping matrix..."
        #now, let's apply the transform to all triangles.
        #Copying values...
        for x in xrange(width):
                    for y in xrange(height):
                        #find the corresponding triangle in dest image
                        simplex = tesselation.find_simplex((x,y))
                        if simplex == -1:
                            #coordinate out of mesh and not in any triangle
                            cv.Set2D(mapx, y, x, -1)
                            cv.Set2D(mapy, y, x, -1)
                            continue

                        u_array = t_maps[simplex][0]
                        v_array = t_maps[simplex][1]
                        cv.Set2D(mapx, y, x, float(u_array[y][x]))
                        cv.Set2D(mapy, y, x, float(v_array[y][x]))

        #Do a remap
        final_image = cv.CreateImage((width,height), 8, 3)
        cv.Remap(src_image, final_image, mapx, mapy)

        return final_image

    def piecewise_tps_time(self, ratio):
        between_ptlist = self.interpolate_points(self.src_ptlist, self.dest_ptlist, ratio)
        warp_image = self.piecewise_tps_warp(self.src_image, self.src_ptlist, between_ptlist)
        inv_warp_image = self.piecewise_tps_warp(self.dest_image, self.dest_ptlist, between_ptlist)

        final_image = self.color_interpolate(warp_image, inv_warp_image, self.color_function(ratio))

        return final_image
        #return self.piecewise_tps_warp(self.src_image, self.src_ptlist, self.dest_ptlist)

    def morphing_time(self, ratio):
        """Main transformation function"""
        transform = self.transform_type

        if transform == Transform.PW_TPS:
            return self.piecewise_tps_time(ratio)
        elif transform == Transform.PW_AFFINE:
            return self.piecewise_affine_time(ratio)
        elif transform == Transform.AFFINE:
            return self.affine_time(ratio)
        elif transform == Transform.TPS:
            return self.tps_time(ratio)
        elif transform == Transform.POLY:
            return self.poly_time(ratio)
        elif transform == Transform.PW_POLY:
            return self.piecewise_poly_time(ratio)

    def write_video(self, outvid, step = 0.01):
        ratio = 0
        print "Writing video file..."
        while ratio <= 1:
            frame = self.morphing_time(ratio)
            cv.WriteFrame(outvid, frame)
            ratio += step
            print ratio,


if __name__ == '__main__':
    main()
